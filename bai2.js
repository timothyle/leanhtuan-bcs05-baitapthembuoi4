function tinhNgay() {
    const sothang = document.getElementById("thang").value*1;
    const sonam = document.getElementById("nam").value*1;

    if (sothang > 12) {
        document.getElementById("result").innerHTML = "Lỗi tháng";
    } else if (sothang == 1 || sothang == 3 || sothang == 5 || sothang == 7 || sothang == 8 || sothang == 10|| sothang == 12) {
        document.getElementById("result").innerHTML = `Tháng ${sothang} có 31 ngày`;
    } else if (sothang == 4 || sothang == 6 || sothang == 9 || sothang == 11) {
        document.getElementById("result").innerHTML = `Tháng ${sothang} có 30 ngày`;
    } else if (sothang == 2 && (sonam % 100) == 0) {
        document.getElementById("result").innerHTML = `Tháng ${sothang} có 29 ngày`;
    }  else if (sothang == 2 && sonam / 4 && (sonam % 100) != 0 || ((sonam % 400) == 0)) {
        document.getElementById("result").innerHTML = `Tháng ${sothang} có 28 ngày`;
    } 
}