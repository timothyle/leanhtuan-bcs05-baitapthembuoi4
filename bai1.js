/** 
 * Bước 1: nhập lấy nguyên tố ngày, tháng, năm;
 * Bước 2: xét năm nhuận, tháng dưới 31 ngày và 1 năm chỉ có 12 tháng
 * Bước 3: in kết quả
*/

function ngayHomQua() {
  
    const songay = document.getElementById("ngay").value *1;
    const sothang = document.getElementById("thang").value*1;
    const sonam = document.getElementById("nam").value*1;

    if (songay > 31 && sothang > 12 ) {
        document.getElementById("result1").innerHTML = "Lỗi ngày và tháng";
    } else if (songay == 1 && ((sothang == 5) || (sothang == 7) || (sothang == 10)|| (sothang == 12)) ) {
        document.getElementById("result1").innerHTML = `"30/${sothang-1}/${sonam}`;
    } else if (songay == 1 && ((sothang == 2) || (sothang == 4)|| (sothang == 6)|| (sothang == 8)|| (sothang == 9)|| (sothang == 11)) ) {
        document.getElementById("result1").innerHTML = `"31/${sothang-1}/${sonam}`;
    } else if (songay == 1 && sothang == 3 && ((sonam % 100)==0)) {
        document.getElementById("result1").innerHTML = `29/${sothang-1}/${sonam}`;
    }  else if (songay == 1 && sothang == 3 && ((sonam / 4 && (sonam % 100) != 0) || ((sonam % 400) == 0))) {
        document.getElementById("result1").innerHTML = `28/${sothang-1}/${sonam}`;
    } else if (songay == 1 && sothang == 1) {
        document.getElementById("result1").innerHTML = `31/12/${sonam-1}`;
    } else if (sothang >= 1 && songay > 1) {
        document.getElementById("result1").innerHTML = `${songay-1}/${sothang}/${sonam}`;
    }
}
function ngayMai() {
  
    const songay = document.getElementById("ngay").value *1;
    const sothang = document.getElementById("thang").value*1;
    const sonam = document.getElementById("nam").value*1;

    if (songay > 31 && sothang > 12 ) {
        document.getElementById("result2").innerHTML = "Lỗi ngày và tháng";
    } else if (sothang > 1 && songay >= 1) {
        document.getElementById("result2").innerHTML = `${songay+1}/${sothang}/${sonam}`;
    }  else if (songay == 30 && ((sothang == 4) || (sothang == 6) || (sothang == 9)|| (sothang == 11)) ) {
        document.getElementById("result2").innerHTML = `1/${sothang+1}/${sonam}`;
    } else if (songay == 31 && ((sothang == 1) || (sothang == 3)|| (sothang == 5)|| (sothang == 7)|| (sothang == 8)|| (sothang == 10)) ) {
        document.getElementById("result2").innerHTML = `1/${sothang+1}/${sonam}`;
    } else if (songay == 31 && sothang == 12) {
        document.getElementById("result2").innerHTML = `1/1/${sonam+1}`;
    }  else if (songay == 28 && sothang == 2 && ((sonam % 100)==0)) {
        document.getElementById("result2").innerHTML = `1/3/${sonam}`;
    }   else if (songay == 28 && sothang == 2 && ((sonam / 4 && (sonam % 100) != 0) || ((sonam % 400) == 0))) {
        document.getElementById("result2").innerHTML = `29/2/${sonam}`;
    }  else if (songay == 29 && sothang == 2 && ((sonam / 4 && (sonam % 100) != 0) || ((sonam % 400) == 0))) {
        document.getElementById("result2").innerHTML = `1/3/${sonam}`;
    } 
}

